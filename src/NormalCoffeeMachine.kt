// Máquina de café normal

interface CoffeeMachine {
    fun makeSmallCoffee()
    fun makeLargeCoffee()
}

class NormalCoffeeMachine : CoffeeMachine {
    override fun makeSmallCoffee() = println("Normal: Small coffee")
    override fun makeLargeCoffee() = println("Normal: Large coffee")
}
fun main(args: Array<String>)
{
    decorator()
}

fun decorator() {
    val normalMachine = NormalCoffeeMachine()
    val enhancedMachine = EnhancedCoffeeMachine(normalMachine)

    enhancedMachine.makeSmallCoffee()
    enhancedMachine.makeLargeCoffee()
    enhancedMachine.makeCoffeeWithMilk()
}